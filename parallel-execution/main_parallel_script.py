from dextor.job import ParallelExecutor
from dextor.job import TimeUnit
import socket

if __name__ == '__main__':
    parallel_executor = ParallelExecutor(profile='dextor')
    time_unit = TimeUnit()
    '''
    start() will start a new batch of executions in parallel
    '''
    print("Script Ran on Slave : " + str(socket.gethostname()))
    parallel_executor.start()
    
    parallel_executor.execute(
        script_name="first_slave.py", script_version="1.0")
    parallel_executor.execute(
        script_name="first_slave.py", script_version="1.0")
    parallel_executor.execute(
        script_name="first_slave.py", script_version="1.0")
    parallel_executor.execute(
        script_name="first_slave.py", script_version="1.0")
    parallel_executor.execute(
        script_name="first_slave.py", script_version="1.0")
    parallel_executor.execute(
        script_name="first_slave.py", script_version="1.0")
    parallel_executor.execute(
        script_name="first_slave.py", script_version="1.0")
    parallel_executor.execute(
        script_name="first_slave.py", script_version="1.0")
    parallel_executor.execute(
        script_name="first_slave.py", script_version="1.0")
    parallel_executor.execute(
        script_name="first_slave.py", script_version="1.0")
    parallel_executor.execute(
        script_name="first_slave.py", script_version="1.0")
    parallel_executor.execute(
        script_name="first_slave.py", script_version="1.0")
    parallel_executor.execute(
        script_name="first_slave.py", script_version="1.0")
    parallel_executor.execute(
        script_name="first_slave.py", script_version="1.0")

    '''
    await_termination() will wait till all executions
    are completed or till elapsed time which ever is earlier
    '''
    job_dict = parallel_executor.await_termination(10, time_unit.MINUTES)
    #job_dict = parallel_executor.await_termination(10, time_unit.MINUTES)

    for key, value in job_dict.items():
        print("Job : " + key)
        print("Status : " + value)
        print("Output : " + str(parallel_executor.output(key)))
        print("~~~~~~~~~~~~~~~~~~~")
