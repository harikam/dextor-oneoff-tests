
import os, sys, time
import collections
import json

job_path = sys.argv[1]
input_path = job_path+"INPUT"
os.chdir(input_path)
files = [f for f in os.listdir(".")]
data_name = files[0]
input_json = json.load(open(data_name))
    
print(input_json)