import sys
import csv

def create_output_file(job_path):
	output_file = job_path  + "/OUTPUT/test.csv"

	with open(output_file, 'w') as csvfile:
		filewriter = csv.writer(csvfile, delimiter=',',
														quotechar='|', quoting=csv.QUOTE_MINIMAL)
		filewriter.writerow(['Name', 'Profession'])
		filewriter.writerow(['Harry', 'Chef'])

if __name__== '__main__':
	create_output_file(sys.argv[1])
