from PIL import Image
import sys

def create_image(input_path):
  output_file = input_path  + "/OUTPUT/test.png"
  img = Image.new('RGB', (60, 30), color = 'red')
  img.save(output_file)

if __name__== '__main__':
  create_image(sys.argv[1])